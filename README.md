# Homie MQTT wall switch

Right now there is support for:
* SONOFF Touch Wall switches 1-3 Gang
* MEAMOR Touch Wall switches 1-3 Gang
* MOES Wall switches 1-3 Gang

Firmware which exposes the touch buttons LEDs and relais activation of these devices to an MQTT Broker.

The Firmware uses the Homie 3.0.1 convention so auto detection in OpenHab is working (tested with Version 3.0 and 2.5.9).

[![works with MQTT Homie](https://homieiot.github.io/img/works-with-homie.svg "works with MQTT Homie")](https://homieiot.github.io/)

## Download

The Git repository contains the development version of these firmware. It is possible that there are some bugs especially if you would like to use the 2-Gang and 1-Gang version which I haven't tested by myself.
## license
- https://creativecommons.org/licenses/by-nc-sa/3.0/ CC BY-NC-SA 3.0
- Use at your own risk
- everything is tested as good as I can but I couldn't give any guarantee
## Using with PlatformIO

[PlatformIO](http://platformio.org) is an open source ecosystem for IoT development with cross platform build system, library manager and full support for Espressif ESP8266 development. It works on the popular host OS: Mac OS X, Windows, Linux 32/64, Linux ARM (like Raspberry Pi, BeagleBone, CubieBoard).

1. Install [PlatformIO IDE](http://platformio.org/platformio-ide)
2. Create new project using "PlatformIO Home > New Project"
3. Open [Project Configuration File `platformio.ini`](http://docs.platformio.org/page/projectconf.html)


-----
Happy coding with PlatformIO!

## Features

* Automatic connection/reconnection to Wi-Fi/MQTT
* exposes up to three buttons/LEDs/relays to homie
* each button is able to recognise 6 different input actions: SINGLE, DOUBLE, TRIPLE, SINGLEHELD, DOUBLEHELD, TRIPLEHELD
* each button gives the state: NONE after relasing an short tap action.
* each button gives the state: RELEASED after an held action.
* the button LEDs are not controlled by the device itself! They are controlled by the overlaying automation system (OpenHab, ...)
* the relays are not controlled by the device itself! They are controlled by the overlaying automation system (OpenHab, ...)
* it is possible to let each LED/relay turn on/off from the overlaying automation system
* it is possible to let each LED blink indiviually by a given frequency and an given amount how often the LED should blink
* not tested feature Homie OTA update - I couldn't get the firmware update server to work the firmware includes the magic bytes

## How to install
* information's comes later. I desoldered one pin inside of the Touch switch and added an pin header for the RX/TX/reset/GND and 5V lines to be able to flash with USB2TTL Uart device.

## Credits/thanks to the following projects
* https://github.com/homieiot/homie-esp8266
* https://tasmota.github.io/docs/devices/TYWE3S/
* https://tasmota.github.io/docs/TuyaMCU-Devices/
* https://github.com/ct-Open-Source/tuya-convert

## The device configuration to the MQTT broker shows the following content

* homie/<device-name>/Switch/$name Button
* homie/<device-name>/Switch/$type Button Press Detection
* homie/<device-name>/Switch/$properties on,button1,button2,button3
* homie/<device-name>/Switch/on true
* homie/<device-name>/Switch/on/$name on/off Button press
* homie/<device-name>/Switch/on/$settable true
* homie/<device-name>/Switch/on/$datatype boolean
* homie/<device-name>/Switch/button1/$name Button 1
* homie/<device-name>/Switch/button1/$settable true
* homie/<device-name>/Switch/button1/$datatype enum
* homie/<device-name>/Switch/button1/$format NONE,RELEASED,SINGLE,DOUBLE,TRIPLE,SINGLEHELD,DOUBLEHELD,TRIPLEHELD
* homie/<device-name>/Switch/button2/$name Button 2
* homie/<device-name>/Switch/button2/$settable true
* homie/<device-name>/Switch/button2/$datatype enum
* homie/<device-name>/Switch/button2/$format NONE,RELEASED,SINGLE,DOUBLE,TRIPLE,SINGLEHELD,DOUBLEHELD,TRIPLEHELD
* homie/<device-name>/Switch/button3/$name Button 3
* homie/<device-name>/Switch/button3/$settable true
* homie/<device-name>/Switch/button3/$datatype enum
* homie/<device-name>/Switch/button3/$format NONE,RELEASED,SINGLE,DOUBLE,TRIPLE,SINGLEHELD,DOUBLEHELD,TRIPLEHELD
* homie/<device-name>/SwitchRelais/$name Relais
* homie/<device-name>/SwitchRelais/$properties on1,on2,on3
* homie/<device-name>/SwitchRelais/$type Relais Output
* homie/<device-name>/SwitchRelais/on1 false
* homie/<device-name>/SwitchRelais/on1/$name on/off Relais 1
* homie/<device-name>/SwitchRelais/on1/$settable true
* homie/<device-name>/SwitchRelais/on1/$datatype boolean
* homie/<device-name>/SwitchRelais/on2 false
* homie/<device-name>/SwitchRelais/on2/$name on/off Relais 2
* homie/<device-name>/SwitchRelais/on2/$settable true
* homie/<device-name>/SwitchRelais/on2/$datatype boolean
* homie/<device-name>/SwitchRelais/on3 false
* homie/<device-name>/SwitchRelais/on3/$name on/off Relais 3
* homie/<device-name>/SwitchRelais/on3/$settable true
* homie/<device-name>/SwitchRelais/on3/$datatype boolean
* homie/<device-name>/SwitchLEDs/$name LEDs
* homie/<device-name>/SwitchLEDs/$properties ledinvert1,ledblink1,ledblinktimes1,ledonblinkfrequency1,ledinvert2,ledblink2,ledblinktimes2,ledonblinkfrequency2,ledinvert3,ledblink3,ledblinktimes3,ledonblinkfrequency3
* homie/<device-name>/SwitchLEDs/$type Relais Output
* homie/<device-name>/SwitchLEDs/ledinvert1 true
* homie/<device-name>/SwitchLEDs/ledinvert1/$name invert LED Button 1
* homie/<device-name>/SwitchLEDs/ledinvert1/$settable true
* homie/<device-name>/SwitchLEDs/ledinvert1/$datatype boolean
* homie/<device-name>/SwitchLEDs/ledblink1 false
* homie/<device-name>/SwitchLEDs/ledblink1/$name Blink LED Button 1
* homie/<device-name>/SwitchLEDs/ledblink1/$settable true
* homie/<device-name>/SwitchLEDs/ledblink1/$datatype boolean
* homie/<device-name>/SwitchLEDs/ledblinktimes1 1
* homie/<device-name>/SwitchLEDs/ledblinktimes1/$name how often the LED Button 1 should blink
* homie/<device-name>/SwitchLEDs/ledblinktimes1/$settable true
* homie/<device-name>/SwitchLEDs/ledblinktimes1/$datatype integer
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency1 200
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency1/$name blink frequency of LED Button 1 [%u ms]
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency1/$settable true
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency1/$datatype integer
* homie/<device-name>/SwitchLEDs/ledinvert2 true
* homie/<device-name>/SwitchLEDs/ledinvert2/$name invert LED Button 2
* homie/<device-name>/SwitchLEDs/ledinvert2/$settable true
* homie/<device-name>/SwitchLEDs/ledinvert2/$datatype boolean
* homie/<device-name>/SwitchLEDs/ledinvert3 true
* homie/<device-name>/SwitchLEDs/ledinvert3/$name invert LED Button 3
* homie/<device-name>/SwitchLEDs/ledinvert3/$settable true
* homie/<device-name>/SwitchLEDs/ledinvert3/$datatype boolean
* homie/<device-name>/SwitchLEDs/ledblink2 false
* homie/<device-name>/SwitchLEDs/ledblink2/$name Blink LED Button 2
* homie/<device-name>/SwitchLEDs/ledblink2/$settable true
* homie/<device-name>/SwitchLEDs/ledblink2/$datatype boolean
* homie/<device-name>/SwitchLEDs/ledblinktimes2 65535
* homie/<device-name>/SwitchLEDs/ledblinktimes2/$name how often the LED Button 2 should blink
* homie/<device-name>/SwitchLEDs/ledblinktimes2/$settable true
* homie/<device-name>/SwitchLEDs/ledblinktimes2/$datatype integer
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency2 100
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency2/$name blink frequency of LED Button 2 [%u ms]
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency2/$settable true
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency2/$datatype integer
* homie/<device-name>/SwitchLEDs/ledblink3 false
* homie/<device-name>/SwitchLEDs/ledblink3/$name Blink LED Button 3
* homie/<device-name>/SwitchLEDs/ledblink3/$settable true
* homie/<device-name>/SwitchLEDs/ledblink3/$datatype boolean
* homie/<device-name>/SwitchLEDs/ledblinktimes3 1
* homie/<device-name>/SwitchLEDs/ledblinktimes3/$name how often the LED Button 3 should blink
* homie/<device-name>/SwitchLEDs/ledblinktimes3/$settable true
* homie/<device-name>/SwitchLEDs/ledblinktimes3/$datatype integer
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency3 200
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency3/$name blink frequency of LED Button 3 [%u ms]
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency3/$settable true
* homie/<device-name>/SwitchLEDs/ledonblinkfrequency3/$datatype integer
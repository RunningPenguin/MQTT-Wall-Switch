/*
 * LEDStateNode.hpp
 * Homie node for a cheap chinese PIR Motion sensor
 *
 * Version: 1.0
 * Author: Tobias Sachs (http://github.com/luebbe)
 */

#include <LEDStateNode.hpp>



#define OUTPUT_PIN1 LED_BUTTON_OUTPUT_PIN1
#define OUTPUT_PIN2 LED_BUTTON_OUTPUT_PIN2
#define OUTPUT_PIN3 LED_BUTTON_OUTPUT_PIN3


LEDStateNode::LEDStateNode(const char *id, const char *name)
    : HomieNode(id, name, "Relais Output")
{
}

int32_t LEDStateNode::tryStrToInt(const String &value, const int maxvalue)
{
  return constrain(value.toInt(), 0, maxvalue);
}

bool LEDStateNode::handleInput(const HomieRange &range, const String &property, const String &value)
{
  if (property == cPropInvert1)
  {
    digitalWrite(OUTPUT_PIN1, !digitalRead(OUTPUT_PIN1));
    returnLedOnOff1ToOpenhab();
//    if (value == "true")
//    {
//        digitalWrite(OUTPUT_PIN1, LOW);
//        setProperty(cPropInvert1).send("true");
//    }
//    if (value == "false")
//    {
//        digitalWrite(OUTPUT_PIN1, HIGH);
//        setProperty(cPropInvert1).send("false");
//    }
  }
  else if (property == cPropBlink1)
  {
    if (value == "true")
    {
        //digitalWrite(OUTPUT_PIN1, !digitalRead(OUTPUT_PIN1)); //==LED Toggle
        setProperty(cPropBlink1).send("true");
        ledState[0]=digitalRead(OUTPUT_PIN1);

        lockLowLed[0] = 0;
        MillisActivateReset[0] = millis();
    }
    if (value == "false")
    {
        //digitalWrite(OUTPUT_PIN1, HIGH);
        lockLowLed[0] = blinkTimes[0];
        digitalWrite(OUTPUT_PIN1, ledState[0]);
        setProperty(cPropBlink1).send("false");
        returnLedOnOff1ToOpenhab();
    }
  }
  else if (property == cPropBlinkTimes1)
  {
    blinkTimes[0] = (uint32_t) tryStrToInt(value, 65535); //maximum 65535 blinks
    setProperty(cPropBlinkTimes1).send(String(blinkTimes[0]));
    blinkTimes[0]= ((blinkTimes[0]*2)/*-1*/); //(blinkTimes[0]*2+1) *2 because we toggle the LED here and in the message we send how often it should be lit up -1 to get the state off at last
    lockLowLed[0] = blinkTimes[0];  //to prevent the LEDs from blinking after the change of the blink times
  }
  else if (property == cPropBlinkfrequency1)
  {
    blinkFrequency[0] = (uint32_t) tryStrToInt(value, 3000);
    setProperty(cPropBlinkfrequency1).send(String(blinkFrequency[0]));
  }
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  else if (property == cPropInvert2)
  {
    digitalWrite(OUTPUT_PIN2, !digitalRead(OUTPUT_PIN2));
    returnLedOnOff2ToOpenhab();
//    if (value == "true")
//    {
//        digitalWrite(OUTPUT_PIN2, LOW);
//        setProperty(cPropInvert2).send("true");
//    }
//    if (value == "false")
//    {
//        digitalWrite(OUTPUT_PIN2, HIGH);
//        setProperty(cPropInvert2).send("false");
//    }
  }
  else if (property == cPropBlink2)
  {
    if (value == "true")
    {
        //digitalWrite(OUTPUT_PIN2, !digitalRead(OUTPUT_PIN2)); //==LED Toggle
        setProperty(cPropBlink2).send("true");
        ledState[1]=digitalRead(OUTPUT_PIN2);

        lockLowLed[1] = 0;
        MillisActivateReset[1] = millis();
    }
    if (value == "false")
    {
        //digitalWrite(OUTPUT_PIN2, HIGH);
        lockLowLed[1] = blinkTimes[1];
        digitalWrite(OUTPUT_PIN2, ledState[1]);
        setProperty(cPropBlink2).send("false");
        returnLedOnOff2ToOpenhab();
    }
  }
  else if (property == cPropBlinkTimes2)
  {
    blinkTimes[1] = (uint32_t) tryStrToInt(value, 65535); 
    setProperty(cPropBlinkTimes2).send(String(blinkTimes[1]));
    blinkTimes[1]= ((blinkTimes[1]*2)/*-1*/);
    lockLowLed[1] = blinkTimes[1];
  }
  else if (property == cPropBlinkfrequency2)
  {
    blinkFrequency[1] = (uint32_t) tryStrToInt(value, 3000);
    setProperty(cPropBlinkfrequency2).send(String(blinkFrequency[1]));
  }
#endif
#if defined THREE_BUTTONS
  else if (property == cPropInvert3)
  {
    digitalWrite(OUTPUT_PIN3, !digitalRead(OUTPUT_PIN3));
    returnLedOnOff3ToOpenhab();
//    if (value == "true")
//    {
//        digitalWrite(OUTPUT_PIN3, LOW);
//        setProperty(cPropInvert3).send("true");
//    }
//    if (value == "false")
//    {
//        digitalWrite(OUTPUT_PIN3, HIGH);
//        setProperty(cPropInvert3).send("false");
//    }
  }
  else if (property == cPropBlink3)
  {
    if (value == "true")
    {
        //digitalWrite(OUTPUT_PIN3, !digitalRead(OUTPUT_PIN3)); //==LED Toggle
        setProperty(cPropBlink3).send("true");
        ledState[2]=digitalRead(OUTPUT_PIN3);

        lockLowLed[2] = 0;
        MillisActivateReset[2] = millis();
    }
    if (value == "false")
    {
        //digitalWrite(OUTPUT_PIN3, HIGH);
        lockLowLed[2] = blinkTimes[2];
        digitalWrite(OUTPUT_PIN3, ledState[2]);
        setProperty(cPropBlink3).send("false");
        returnLedOnOff3ToOpenhab();
    }
  }
  else if (property == cPropBlinkTimes3)
  {
    blinkTimes[2] = (uint32_t) tryStrToInt(value, 65535);
    setProperty(cPropBlinkTimes3).send(String(blinkTimes[2]));
    blinkTimes[2] = ((blinkTimes[2]*2)/*-1*/);
    lockLowLed[2] = blinkTimes[2];
  }
  else if (property == cPropBlinkfrequency3)
  {
    blinkFrequency[2] = (uint32_t) tryStrToInt(value, 3000);
    setProperty(cPropBlinkfrequency3).send(String(blinkFrequency[2]));
  }
#endif

  return true;
}


void LEDStateNode::loop()
{
  resetAfterShortTime();
}


void LEDStateNode::beforeSetup()
{
  advertise(cPropInvert1).setName("invert LED Button 1").setDatatype("boolean").settable();         // on/off = true/false
  advertise(cPropBlink1).setName("Blink LED Button 1").setDatatype("boolean").settable();       // on/off = true/false
  advertise(cPropBlinkTimes1).setName("how often the LED Button 1 should blink").setDatatype("integer").settable();     // unsigned integer how often the LED should blink
  advertise(cPropBlinkfrequency1).setName("blink frequency of LED Button 1 [%u ms]").setDatatype("integer").settable();         // unsigned integer of the frequency with which the LED should blink
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  advertise(cPropInvert2).setName("invert LED Button 2").setDatatype("boolean").settable();         // on/off = true/false
  advertise(cPropBlink2).setName("Blink LED Button 2").setDatatype("boolean").settable();         // on/off = true/false
  advertise(cPropBlinkTimes2).setName("how often the LED Button 2 should blink").setDatatype("integer").settable();
  advertise(cPropBlinkfrequency2).setName("blink frequency of LED Button 2 [%u ms]").setDatatype("integer").settable();
#endif
#if defined THREE_BUTTONS
  advertise(cPropInvert3).setName("invert LED Button 3").setDatatype("boolean").settable();         // on/off = true/false
  advertise(cPropBlink3).setName("Blink LED Button 3").setDatatype("boolean").settable();         // on/off = true/false
  advertise(cPropBlinkTimes3).setName("how often the LED Button 3 should blink").setDatatype("integer").settable();
  advertise(cPropBlinkfrequency3).setName("blink frequency of LED Button 3 [%u ms]").setDatatype("integer").settable();
#endif
}


void LEDStateNode::setup()
{
  pinMode(OUTPUT_PIN1, OUTPUT);
  digitalWrite(OUTPUT_PIN1, HIGH);

#if defined TWO_BUTTONS || defined THREE_BUTTONS
  pinMode(OUTPUT_PIN2, OUTPUT);
  digitalWrite(OUTPUT_PIN2, HIGH);
#endif
#if defined THREE_BUTTONS
  pinMode(OUTPUT_PIN3, OUTPUT);
  digitalWrite(OUTPUT_PIN3, HIGH);
#endif
}


void LEDStateNode::returnLedOnOff1ToOpenhab()
{
  if(digitalRead(OUTPUT_PIN1) == LOW) setProperty(cPropInvert1).send("true");
  if(digitalRead(OUTPUT_PIN1) == HIGH) setProperty(cPropInvert1).send("false");
}

void LEDStateNode::returnLedOnOff2ToOpenhab()
{
  if(digitalRead(OUTPUT_PIN2) == LOW) setProperty(cPropInvert2).send("true");
  if(digitalRead(OUTPUT_PIN2) == HIGH) setProperty(cPropInvert2).send("false");
}

void LEDStateNode::returnLedOnOff3ToOpenhab()
{
  if(digitalRead(OUTPUT_PIN3) == LOW) setProperty(cPropInvert3).send("true");
  if(digitalRead(OUTPUT_PIN3) == HIGH) setProperty(cPropInvert3).send("false");
}

void LEDStateNode::returnBlink1ToOpenhab()
{
  returnLedOnOff1ToOpenhab();
  if(( blinkTimes[0] - lockLowLed[0]) != 0) setProperty(cPropBlink1).send("true");
  else setProperty(cPropBlink1).send("false");
}

void LEDStateNode::returnBlink2ToOpenhab()
{
  returnLedOnOff2ToOpenhab();
  if(( blinkTimes[1] - lockLowLed[1]) != 0) setProperty(cPropBlink2).send("true");
  else setProperty(cPropBlink2).send("false");
}

void LEDStateNode::returnBlink3ToOpenhab()
{
  returnLedOnOff3ToOpenhab();
  if(( blinkTimes[2] - lockLowLed[2]) != 0) setProperty(cPropBlink3).send("true");
  else setProperty(cPropBlink3).send("false");
}

void LEDStateNode::returnStatusToOpenhab()
{
  setProperty(cPropBlinkTimes1).send(String(blinkTimes[0]));
  returnBlink1ToOpenhab();
  setProperty(cPropBlinkfrequency1).send(String(blinkFrequency[0]));

#if defined TWO_BUTTONS || defined THREE_BUTTONS
  setProperty(cPropBlinkTimes2).send(String(blinkTimes[1]));
  returnBlink2ToOpenhab();
  setProperty(cPropBlinkfrequency2).send(String(blinkFrequency[1]));
#endif

#if defined THREE_BUTTONS
  setProperty(cPropBlinkTimes3).send(String(blinkTimes[2]));
  returnBlink3ToOpenhab();
  setProperty(cPropBlinkfrequency3).send(String(blinkFrequency[2]));
#endif
}


void LEDStateNode::resetAfterShortTime()
{

  if(millis() - MillisActivateReset[0] > blinkFrequency[0] && ( blinkTimes[0] - lockLowLed[0]) != 0)
  {
    MillisActivateReset[0] = millis();
    digitalWrite(OUTPUT_PIN1, !digitalRead(OUTPUT_PIN1)); //toglle LED output
    lockLowLed[0] ++;
    returnBlink1ToOpenhab();
  }
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  if(millis() - MillisActivateReset[1] > blinkFrequency[1] && ( blinkTimes[1] - lockLowLed[1]) != 0)
  {
    MillisActivateReset[1] = millis();
    digitalWrite(OUTPUT_PIN2, !digitalRead(OUTPUT_PIN2));
    lockLowLed[1] ++;
    returnBlink2ToOpenhab();
  }
#endif
#if defined THREE_BUTTONS
  if(millis() - MillisActivateReset[2] > blinkFrequency[2] && ( blinkTimes[2] - lockLowLed[2]) != 0)
  {
    MillisActivateReset[2] = millis();
    digitalWrite(OUTPUT_PIN3, !digitalRead(OUTPUT_PIN3));
    lockLowLed[2] ++;
    returnBlink3ToOpenhab();
  }
#endif
}
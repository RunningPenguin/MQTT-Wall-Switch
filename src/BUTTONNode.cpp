/*
 * BUTTONNode.hpp
 * Homie node for a cheap chinese PIR Motion sensor
 *
 * Version: 1.0
 * Author: Tobias Sachs (http://github.com/luebbe)
 */

#include <BUTTONNode.hpp>

#include <ClickButton.h>

// Associate the PIN_BUTTON GPIO with the ClickButton library
ClickButton button1(BUTTON_INPUT_PIN1, LOW, CLICKBTN_PULLUP);
#if defined TWO_BUTTONS || defined THREE_BUTTONS
ClickButton button2(BUTTON_INPUT_PIN2, LOW, CLICKBTN_PULLUP);
#endif
#if defined THREE_BUTTONS
ClickButton button3(BUTTON_INPUT_PIN3, LOW, CLICKBTN_PULLUP);
#endif

BUTTONNode::BUTTONNode(const char *id, const char *name)
    : HomieNode(id, name, "Button Press Detection")
{
}


bool BUTTONNode::handleInput(const HomieRange &range, const String &property, const String &value)
{
  if (property == cPropOn)
  {
    if (value == "true")
    {
        allowButtonPress = true;
        setProperty(cPropOn).send("true");
    }
    if (value == "false")
    {
        allowButtonPress = false;
        setProperty(cPropOn).send("false");
    }
  }
  return true;
}

void BUTTONNode::ProcessButton1()
{
  // Save click codes in function1, as click codes are reset at next Update()
  if(button1.clicks != 0) function1 = button1.clicks;

  // These are single event button presses.
  // Handle them and reset function1 back to 0
  if ( function1 > 0 ) {
    // Serial.println("One-shot");
    if ( function1 == 1 ) {
      // If not connected, toggle the relay like a normal switch.
      // Calls to relayNode don't work whilst offline.
      if ( !Homie.isConnected() ) {
        // Serial.println("Offline toggle of relay");
        digitalWrite(RELAIS_OUTPUT_PIN1, !digitalRead(RELAIS_OUTPUT_PIN1));
      }
      if ( Homie.isConnected() ) {
        // relayNode.setProperty("relayState").send("ON"); //TODO connection to RelaisNode necessary?
        setProperty(cPropButton1).setRetained(false).send("SINGLE");
      }
      // Read the new state, and use it to set the initial startup state
      String PIN_STATE1 = String(digitalRead(RELAIS_OUTPUT_PIN1));
      // The retained initmode state really needs to be set by the HA system
      // Determine if the local bulb is on or off, and set the desired InitMode of the switch
      //relayNode.setProperty("relayInitMode").setRetained(false).send(PIN_STATE);
      // Serial.println("SINGLE click");
    }

    if ( function1 == 2 ) {
      if ( Homie.isConnected() ) { setProperty(cPropButton1).setRetained(false).send("DOUBLE"); }
      // Serial.println("DOUBLE click");
    }

    if ( function1 == 3 ) {
      if ( Homie.isConnected() ) { setProperty(cPropButton1).setRetained(false).send("TRIPLE"); }
      // Serial.println("TRIPLE click");
    }
    // This has been a single event.
    function1 = 0;
    setProperty(cPropButton1).setRetained(false).send("NONE");
  }

  // These are repeat events, where the button is being held down.
  // Handle them, but don't reset function1 back to 0 unless the button is released.
  if ( function1 < 0 ) {
    // if ( millis() - previousMillis1 >= waitInterval1 ) {
    //   previousMillis1 = millis();
      if ( function1 == -1  && lockRepeatSending == false) {
        if ( Homie.isConnected() ) { setProperty(cPropButton1).setRetained(false).send("SINGLEHELD"); }
        // Serial.println("SINGLE LONG click");
        lockRepeatSending = true;
      }

      if ( function1 == -2  && lockRepeatSending == false) {
        if ( Homie.isConnected() ) { setProperty(cPropButton1).setRetained(false).send("DOUBLEHELD"); }
        // Serial.println("DOUBLE LONG click");
        lockRepeatSending = true;
      }

      if ( function1 == -3  && lockRepeatSending == false) {
        if ( Homie.isConnected() ) { setProperty(cPropButton1).setRetained(false).send("TRIPLEHELD"); }
        // Serial.println("TRIPLE LONG click");
        lockRepeatSending = true;
      }
    // }

    // Decide whether the button is being held or not
    // I much prefer the idea at https://www.home-assistant.io/cookbook/dim_and_brighten_lights/
    // Where a pair of scripts are triggered by one message, and then loop until another message halts them.
    if ( button1.depressed == 1 ) {
      // This will need rate limited
      // // Serial.println("Held");
    }
    if ( button1.depressed == 0 ) {
      // // Serial.println("Released");
      function1 = 0;
      lockRepeatSending = false;
      setProperty(cPropButton1).setRetained(false).send("RELEASED");
    }
    // Rate limit...
    delay(5);
  }
}

#if defined TWO_BUTTONS || defined THREE_BUTTONS
void BUTTONNode::ProcessButton2()
{
  // Save click codes in function2, as click codes are reset at next Update()
  if(button2.clicks != 0) function2 = button2.clicks;

  // These are single event button presses.
  // Handle them and reset function2 back to 0
  if ( function2 > 0 ) {
    // Serial.println("One-shot");
    if ( function2 == 1 ) {
      // If not connected, toggle the relay like a normal switch.
      // Calls to relayNode don't work whilst offline.
      if ( !Homie.isConnected() ) {
        // Serial.println("Offline toggle of relay");
        digitalWrite(RELAIS_OUTPUT_PIN2, !digitalRead(RELAIS_OUTPUT_PIN2));
      }
      if ( Homie.isConnected() ) {
        // relayNode.setProperty("relayState").send("ON"); //TODO connection to RelaisNode necessary?
        setProperty(cPropButton2).setRetained(false).send("SINGLE");
      }
      // Read the new state, and use it to set the initial startup state
      String PIN_STATE2 = String(digitalRead(RELAIS_OUTPUT_PIN2));
      // The retained initmode state really needs to be set by the HA system
      // Determine if the local bulb is on or off, and set the desired InitMode of the switch
      //relayNode.setProperty("relayInitMode").setRetained(false).send(PIN_STATE);
      // Serial.println("SINGLE click");
    }

    if ( function2 == 2 ) {
      if ( Homie.isConnected() ) { setProperty(cPropButton2).setRetained(false).send("DOUBLE"); }
      // Serial.println("DOUBLE click");
    }

    if ( function2 == 3 ) {
      if ( Homie.isConnected() ) { setProperty(cPropButton2).setRetained(false).send("TRIPLE"); }
      // Serial.println("TRIPLE click");
    }
    // This has been a single event.
    function2 = 0;
    setProperty(cPropButton2).setRetained(false).send("NONE");
  }

  // These are repeat events, where the button is being held down.
  // Handle them, but don't reset function2 back to 0 unless the button is released.
  if ( function2 < 0 ) {
    // if ( millis() - previousMillis1 >= waitInterval1 ) {
    //   previousMillis1 = millis();
      if ( function2 == -1  && lockRepeatSending == false) {
        if ( Homie.isConnected() ) { setProperty(cPropButton2).setRetained(false).send("SINGLEHELD"); }
        // Serial.println("SINGLE LONG click");
        lockRepeatSending = true;
      }

      if ( function2 == -2  && lockRepeatSending == false) {
        if ( Homie.isConnected() ) { setProperty(cPropButton2).setRetained(false).send("DOUBLEHELD"); }
        // Serial.println("DOUBLE LONG click");
        lockRepeatSending = true;
      }

      if ( function2 == -3  && lockRepeatSending == false) {
        if ( Homie.isConnected() ) { setProperty(cPropButton2).setRetained(false).send("TRIPLEHELD"); }
        // Serial.println("TRIPLE LONG click");
        lockRepeatSending = true;
      }
    // }

    // Decide whether the button is being held or not
    // I much prefer the idea at https://www.home-assistant.io/cookbook/dim_and_brighten_lights/
    // Where a pair of scripts are triggered by one message, and then loop until another message halts them.
    if ( button2.depressed == 1 ) {
      // This will need rate limited
      // // Serial.println("Held");
    }
    if ( button2.depressed == 0 ) {
      // // Serial.println("Released");
      function2 = 0;
      lockRepeatSending = false;
      setProperty(cPropButton2).setRetained(false).send("RELEASED");
    }
    // Rate limit...
    delay(5);
  }
}
#endif

#if defined THREE_BUTTONS
void BUTTONNode::ProcessButton3()
{
  // Save click codes in function3, as click codes are reset at next Update()
  if(button3.clicks != 0) function3 = button3.clicks;

  // These are single event button presses.
  // Handle them and reset function3 back to 0
  if ( function3 > 0 ) {
    // Serial.println("One-shot");
    if ( function3 == 1 ) {
      // If not connected, toggle the relay like a normal switch.
      // Calls to relayNode don't work whilst offline.
      if ( !Homie.isConnected() ) {
        // Serial.println("Offline toggle of relay");
        digitalWrite(RELAIS_OUTPUT_PIN3, !digitalRead(RELAIS_OUTPUT_PIN3));
      }
      if ( Homie.isConnected() ) {
        // relayNode.setProperty("relayState").send("ON"); //TODO connection to RelaisNode necessary?
        setProperty(cPropButton3).setRetained(false).send("SINGLE");
      }
      // Read the new state, and use it to set the initial startup state
      String PIN_STATE3 = String(digitalRead(RELAIS_OUTPUT_PIN3));
      // The retained initmode state really needs to be set by the HA system
      // Determine if the local bulb is on or off, and set the desired InitMode of the switch
      //relayNode.setProperty("relayInitMode").setRetained(false).send(PIN_STATE);
      // Serial.println("SINGLE click");
    }

    if ( function3 == 2 ) {
      if ( Homie.isConnected() ) { setProperty(cPropButton3).setRetained(false).send("DOUBLE"); }
      // Serial.println("DOUBLE click");
    }

    if ( function3 == 3 ) {
      if ( Homie.isConnected() ) { setProperty(cPropButton3).setRetained(false).send("TRIPLE"); }
      // Serial.println("TRIPLE click");
    }
    // This has been a single event.
    function3 = 0;
    setProperty(cPropButton3).setRetained(false).send("NONE");
  }

  // These are repeat events, where the button is being held down.
  // Handle them, but don't reset function3 back to 0 unless the button is released.
  if ( function3 < 0 ) {
    // if ( millis() - previousMillis1 >= waitInterval1 ) {
    //   previousMillis1 = millis();
      if ( function3 == -1 && lockRepeatSending == false) {
        if ( Homie.isConnected() ) { setProperty(cPropButton3).setRetained(false).send("SINGLEHELD"); }
        // Serial.println("SINGLE LONG click");
        lockRepeatSending = true;
      }

      if ( function3 == -2  && lockRepeatSending == false) {
        if ( Homie.isConnected() ) { setProperty(cPropButton3).setRetained(false).send("DOUBLEHELD"); }
        // Serial.println("DOUBLE LONG click");
        lockRepeatSending = true;
      }

      if ( function3 == -3  && lockRepeatSending == false) {
        if ( Homie.isConnected() ) { setProperty(cPropButton3).setRetained(false).send("TRIPLEHELD"); }
        // Serial.println("TRIPLE LONG click");
        lockRepeatSending = true;
      }
    // }

    // Decide whether the button is being held or not
    // I much prefer the idea at https://www.home-assistant.io/cookbook/dim_and_brighten_lights/
    // Where a pair of scripts are triggered by one message, and then loop until another message halts them.
    if ( button3.depressed == 1 ) {
      // This will need rate limited
      // // Serial.println("Held");
    }
    if ( button3.depressed == 0 ) {
      // // Serial.println("Released");
      function3 = 0;
      lockRepeatSending = false;
      setProperty(cPropButton3).setRetained(false).send("RELEASED");
    }
    // Rate limit...
    delay(5);
  }
}
#endif

void BUTTONNode::loop()
{
  if(allowButtonPress == true)
  {
    // Update button state
    button1.Update();
    ProcessButton1();

#if defined TWO_BUTTONS || defined THREE_BUTTONS
    button2.Update();
    ProcessButton2();
#endif
#if defined THREE_BUTTONS
    button3.Update();
    ProcessButton3();
#endif
  }


}


void BUTTONNode::beforeSetup()
{
  advertise(cPropOn).setName("on/off Button press").setDatatype("boolean").settable();         // on/off = true/false
  advertise(cPropButton1).setName("Button 1").setRetained("false").setDatatype("enum").setFormat("NONE,RELEASED,SINGLE,DOUBLE,TRIPLE,SINGLEHELD,DOUBLEHELD,TRIPLEHELD").settable();
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  advertise(cPropButton2).setName("Button 2").setRetained("false").setDatatype("enum").setFormat("NONE,RELEASED,SINGLE,DOUBLE,TRIPLE,SINGLEHELD,DOUBLEHELD,TRIPLEHELD").settable();
#endif
#if defined THREE_BUTTONS
  advertise(cPropButton3).setName("Button 3").setRetained("false").setDatatype("enum").setFormat("NONE,RELEASED,SINGLE,DOUBLE,TRIPLE,SINGLEHELD,DOUBLEHELD,TRIPLEHELD").settable(); 
#endif
}


void BUTTONNode::setup()
{
  // Setup button timers (all in milliseconds / ms)
  // (These are default if not set, but changeable for convenience)
  button1.debounceTime   = 20;   // Debounce timer in ms
  button1.multiclickTime = 250;  // Time limit for multi clicks
  button1.longClickTime  = 1000; // time until "held-down clicks" register

#if defined TWO_BUTTONS || defined THREE_BUTTONS
  button2.debounceTime   = 20;   // Debounce timer in ms
  button2.multiclickTime = 250;  // Time limit for multi clicks
  button2.longClickTime  = 1000; // time until "held-down clicks" register
#endif
#if defined THREE_BUTTONS
  button3.debounceTime   = 20;   // Debounce timer in ms
  button3.multiclickTime = 250;  // Time limit for multi clicks
  button3.longClickTime  = 1000; // time until "held-down clicks" register
#endif
}

void BUTTONNode::returnStatusToOpenhab()
{
  if(allowButtonPress == true) setProperty(cPropOn).send("true");

  if(allowButtonPress == false) setProperty(cPropOn).send("false");

  setProperty(cPropButton1).setRetained(false).send("NONE");
  setProperty(cPropButton2).setRetained(false).send("NONE");
  setProperty(cPropButton3).setRetained(false).send("NONE");
}
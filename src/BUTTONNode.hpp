/*
 * BUTTONNode.hpp
 * Homie node for a cheap chinese PIR Motion sensor
 *
 * Version: 1.0
 * Author: Tobias Sachs (http://github.com/luebbe)
 */

#pragma once

#include <Homie.hpp>
#include "HardwareConfig.h"

// #define DIMMER_1_PIN_RED 15
// #define DIMMER_2_PIN_GREEN 13
// #define DIMMER_3_PIN_BLUE 12
// #define DIMMER_4_PIN_W1 14
// #define DIMMER_5_PIN_W2 4

// #define LED_PIN_GREEN 1
// #define LED_PIN_RED 5



class BUTTONNode : public HomieNode
{
private:
  const char *cCaption = "• Inut:";

  // Settable properties
  const char *cPropOn = "on"; // Not really settable, always reports true, because the controller is connected to a physical switch
  const char *cPropButton1 = "button1";

  int8_t function1 = 0;
  // Timer related for dimming delays
  uint32_t previousMillis1 = 0;
  // Milliseconds to wait per cycle for a held command.
  const uint32_t waitInterval1 = 100;
  bool lockRepeatSending = false;
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  const char *cPropButton2 = "button2";
  int8_t function2 = 0;
  uint32_t previousMillis2 = 0;
  const uint32_t waitInterval2 = 100;
#endif
#if defined THREE_BUTTONS
  const char *cPropButton3 = "button3";
  int8_t function3 = 0;
  uint32_t previousMillis3 = 0;
  const uint32_t waitInterval3 = 100;
#endif

  boolean allowButtonPress = true;

protected:
  virtual bool handleInput(const HomieRange &range, const String &property, const String &value);
  virtual void loop() override;

  virtual void ProcessButton1();
#if defined TWO_BUTTONS || defined THREE_BUTTONS
  virtual void ProcessButton2();
#endif
#if defined THREE_BUTTONS
  virtual void ProcessButton3();
#endif

public:
  BUTTONNode(const char *id, const char *name);

  void beforeSetup();
  void setup();
  void returnStatusToOpenhab();

};
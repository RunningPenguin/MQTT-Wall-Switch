#define FW_NAME "Homie-Wandschalter"
#define FW_VERSION "0.5.2"

#include <Arduino.h>
#include <Homie.h>

/* Magic sequence for Autodetectable Binary Upload */
const char *__FLAGGED_FW_NAME = "\xbf\x84\xe4\x13\x54" FW_NAME "\x93\x44\x6b\xa7\x75";
const char *__FLAGGED_FW_VERSION = "\x6a\x3f\x3e\x0e\xe1" FW_VERSION "\xb0\x30\x48\xd4\x1a";
/* End of magic sequence for Autodetectable Binary Upload */

#include "RELAISNode.hpp"
#include "BUTTONNode.hpp"
#include "LEDStateNode.hpp"
#include "HardwareConfig.h"


/* -------------------------OTA update -------------------*/
// #include <ArduinoOTA.h>
/* -------------------------------------------------------*/

/* --------------Remote debug over telnet ----------------*/
#include <DNSServer.h>
#include <ESP8266mDNS.h>
#include "RemoteDebug.h"        //https://github.com/JoaoLopesF/RemoteDebug
#include "debug.h"
RemoteDebug Debug;
// Host mDNS
#define HOST_NAME "remotedebug-Wandschalter"
bool runOnceAfterWifiNtp = false;
/* -------------------------------------------------------*/
/* --------------Remote debug over telnet ----------------*/
//different output message levels
// debugI("Test debugI %.4f Antwort\n", 42.0f);
// debugD("Test debugD %.4f Antwort\n", 42.0f);
// debugV("Test debugV %.4f Antwort\n", 42.0f);
// debugW("Test debugD %.4f Antwort\n", 42.0f);
// debugE("Test debugV %.4f Antwort\n", 42.0f);
/* -------------------------------------------------------*/



/* -----------------------Button-press -------------------*/
BUTTONNode buttonnode("Switch", "Button");
/* -------------------------------------------------------*/

RELAISNode relaisnode("SwitchRelais", "Relais");

LEDStateNode lednode("SwitchLEDs", "LEDs");

void onHomieEvent(const HomieEvent& event) {
  switch(event.type) {
    case HomieEventType::MQTT_READY:
      buttonnode.returnStatusToOpenhab();
      delay(400);
      relaisnode.returnStatusToOpenhab();
      delay(400);
      lednode.returnStatusToOpenhab();
      delay(400);
      // Do whatever you want when MQTT is connected in normal mode
      break;
  }
}



void setup()
{
  //To Avoid sleeping of modem for DEBUG reasons (why do I get no I2C messages after a period of a few hours?) //TODO Fix needed?
  wifi_set_sleep_type(NONE_SLEEP_T);

/* ----------------MQTT Homie Convention -----------------*/
  Homie_setFirmware(FW_NAME, FW_VERSION);

#if !defined INVERT_LED
  Homie.setLedPin(PIN_LED, LOW);
#else
  Homie.setLedPin(PIN_LED, HIGH);
#endif

  Homie.disableResetTrigger();

  Homie.onEvent(onHomieEvent);
  // Homie.setSetupFunction(setupHandler);
  // Homie.setLoopFunction(loopHandler);

  buttonnode.beforeSetup();
  relaisnode.beforeSetup();
  lednode.beforeSetup();

  Homie.setup();
/* -------------------------------------------------------*/

  buttonnode.setup();
  relaisnode.setup();

/* --------------Remote debug over telnet ----------------*/
    // Register host name in WiFi and mDNS

    String hostNameWifi = HOST_NAME;
    hostNameWifi.concat(".local");
    WiFi.hostname(hostNameWifi);
    if (MDNS.begin(HOST_NAME))
    {
// #if defined LOCAL_DEBUG_REMOTE_TELNET
//         Serial.print("* MDNS responder started. Hostname -> ");
//         Serial.println(HOST_NAME);
// #endif
    }
    MDNS.addService("telnet", "tcp", 23);

    // Initialize the telnet server of RemoteDebug
    Debug.begin(HOST_NAME); // Initiaze the telnet server
    Debug.setResetCmdEnabled(true); // Enable the reset command
    Debug.showProfiler(true); // Profiler (Good to measure times, to optimize codes)
    Debug.showColors(true); // Colors
    //Debug.setCallBackProjectCmds(&processCmdRemoteDebug);
    
    //String helpCmd = "bench1 - Benchmark 1\n";
    //helpCmd.concat("bench2 - Benchmark 2\n");
    //helpCmd.concat("calibrateCurrent - calibrate the current reading to: specified power divided by the specified voltage\n");
    //helpCmd.concat("calibrateVoltage - calibrate the voltage reading to: the specified voltage\n");
    //helpCmd.concat("calibratePower - calibrate the power reading to: the specified power\n");
    //helpCmd.concat("resetCalibration - reset all calibration data to gains of 1\n");
    //helpCmd.concat("getCalibration - prints out all calibration data\n");

    //Debug.setHelpProjectsCmds(helpCmd);

// #if defined LOCAL_DEBUG_REMOTE_TELNET
//     Serial.println("* Arduino RemoteDebug Library");
//     Serial.println("*");
//     Serial.print("* WiFI connected. IP address: ");
//     Serial.println(WiFi.localIP());
// #endif
/* -------------------------------------------------------*/

// /* -------------------------OTA update -------------------*/
//     ArduinoOTA.onStart([]() {
//     // Serial.println("ArduinoOTA start");
//   });
//   ArduinoOTA.onEnd([]() {
//     // Serial.println("\nArduinoOTA end");
//   });
//   ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
//     // Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
//   });
//   ArduinoOTA.onError([](ota_error_t error) {
//     // Serial.printf("Error[%u]: ", error);
//     if (error == OTA_AUTH_ERROR) {
//       // Serial.println("Auth Failed");
//     } else if (error == OTA_BEGIN_ERROR) {
//       // Serial.println("Begin Failed");
//     } else if (error == OTA_CONNECT_ERROR) {
//       // Serial.println("Connect Failed");
//     } else if (error == OTA_RECEIVE_ERROR) {
//       // Serial.println("Receive Failed");
//     } else if (error == OTA_END_ERROR) {
//       // Serial.println("End Failed");
//     }
//   });
//   ArduinoOTA.begin();
// /* -------------------------------------------------------*/



}

/* --------------Remote debug over telnet ----------------*/
void debugSend(const char * format, ...) {

    va_list args;
    va_start(args, format);
    char test[1];
    int len = ets_vsnprintf(test, 1, format, args) + 1;
    char * buffer = new char[len];
    ets_vsnprintf(buffer, len, format, args);
    va_end(args);

    DEBUG_MSG(buffer);
    delete[] buffer;

}
/* -------------------------------------------------------*/


void loop()
{
/* --------------Remote debug over telnet ----------------*/
  // Remote debug over telnet
  Debug.handle();
/* -------------------------------------------------------*/

/* -------------------------OTA update -------------------*/
  // Remote update
  // ArduinoOTA.handle();
/* -------------------------------------------------------*/


  Homie.loop();
}
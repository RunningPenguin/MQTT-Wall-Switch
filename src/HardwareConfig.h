#pragma once

// #define ONE_BUTTON
// #define TWO_BUTTONS
#define THREE_BUTTONS

#define MOES

#if defined SONOFF

#define RELAIS_OUTPUT_PIN1 12 //Relais Pin on Sonoff touch
#if defined TWO_BUTTONS || defined THREE_BUTTONS
#define RELAIS_OUTPUT_PIN2 5 //Relais Pin on Sonoff touch
#define BUTTON_INPUT_PIN2 9 //Touch Button2 Sonoff Touch
#endif
#if defined THREE_BUTTONS
#define RELAIS_OUTPUT_PIN3 4 //Relais Pin on Sonoff touch
#define BUTTON_INPUT_PIN3 10 //Touch Button3 Sonoff Touch
#endif
#define BUTTON_INPUT_PIN1 0 //Touch Button1 Sonoff Touch

#define PIN_LED 13

bool ButtonLedAvailable = false;

#endif //defined SONOFF



#if defined MEAMOR

#define BUTTON_INPUT_PIN1 12 //Touch Button1 MEAMOR TOUCH // oben 0 == gedrückt
#define LED_BUTTON_OUTPUT_PIN1 16
#define RELAIS_OUTPUT_PIN1 13 //Relais Pin on MEAMOR TOUCH //W1
#if defined TWO_BUTTONS || defined THREE_BUTTONS
#define BUTTON_INPUT_PIN2 5 //Touch Button2 MEAMOR TOUCH // links 0 == gedrückt
#define LED_BUTTON_OUTPUT_PIN2 1
#define RELAIS_OUTPUT_PIN2 15 //Relais Pin on MEAMOR TOUCH //W2
#endif
#if defined THREE_BUTTONS
#define BUTTON_INPUT_PIN3 3 //Touch Button3 MEAMOR TOUCH // rechts 0 == gedrückt
#define LED_BUTTON_OUTPUT_PIN3 14
#define RELAIS_OUTPUT_PIN3 4 //Relais Pin on MEAMOR TOUCH //W3
#endif

#define PIN_LED 0

#define BUTTON_LED_AVAILABLE


#endif //defined MEAMOR



#if defined MOES

#define BUTTON_INPUT_PIN1 12       //Button left MOES Wall Switch 0 == gedrückt
#define LED_BUTTON_OUTPUT_PIN1 16  // left LED 0 == blue, 1 == red
#define RELAIS_OUTPUT_PIN1 13      //Relais Pin on MOES Wall Switch
#if defined TWO_BUTTONS || defined THREE_BUTTONS
#define BUTTON_INPUT_PIN2 3        //Button middle MOES Wall Switch 0 == gedrückt
#define LED_BUTTON_OUTPUT_PIN2 14  // middle LED 0 == blue, 1 == red
#define RELAIS_OUTPUT_PIN2 4       //Relais Pin on MOES Wall Switch
#endif
#if defined THREE_BUTTONS
#define BUTTON_INPUT_PIN3 5        //Button right MOES Wall Switch // left 0 == gedrückt
#define LED_BUTTON_OUTPUT_PIN3 2   // right LED 0 == blue, 1 == red
#define RELAIS_OUTPUT_PIN3 15      //Relais Pin on MOES Wall Switch
#endif

#define PIN_LED 0
#define INVERT_LED

#define BUTTON_LED_AVAILABLE


#endif //defined MOES